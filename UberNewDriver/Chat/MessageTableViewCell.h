#import <UIKit/UIKit.h>

@interface MessageTableViewCell : UITableViewCell
{
    id cellParent;
}

@property (nonatomic, readwrite, strong) IBOutlet UITextView *message;
@property (nonatomic, readwrite, strong) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIView *bubbleView;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImage;
@property (weak, nonatomic) IBOutlet UIImageView *bubbleImageRight;

- (void)setCellData:(NSDictionary *)dictData withParent:(id)parent ;
- (void)setHeight;
- (CGFloat)getHeight;

@end
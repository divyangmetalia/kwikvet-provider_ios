//
//  ChatVC.h
//  KwikVet
//
//  Created by Elluminati Macbook Pro 2 on 11/22/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import "BaseVC.h"

@interface ChatVC : BaseVC<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic,strong) NSDictionary *dictHistoryInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblProviderName;
@property (weak, nonatomic) IBOutlet UITextField *txtMessage;
@property (weak, nonatomic) IBOutlet UIView *viewForKeyboard;

@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoChats;

@property (weak, nonatomic) IBOutlet UIButton *btnChat;
@property (nonatomic , strong) NSString *strClient;
@property (nonatomic , strong) NSString *strFollowUpProviderId;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickSend:(id)sender;

@property (weak, nonatomic)NSTimer *timerForChat;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tableForChat;

@end

#import "MessageTableViewCell.h"

@implementation MessageTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.lblMessage setTextColor:[UIColor blackColor]];
}

- (void)setCellData:(NSDictionary *)dictData withParent:(id)parent
{
    if(dictData != nil)
    {
        NSString *strMessage = [dictData valueForKey:@"msg"];
        
        NSData *strData = [strMessage dataUsingEncoding:NSUTF8StringEncoding];
        
        NSString *strOutgoing = [[NSString alloc] initWithData:strData encoding:NSNonLossyASCIIStringEncoding];
        
        self.bubbleView.hidden = NO;
        
        cellParent = parent;
        
        NSString *trimmed = [strOutgoing stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        self.lblMessage.text = trimmed;
        
        [self setHeight];
    }
    else
    {
        self.bubbleView.hidden = YES;
    }
}

- (void)setHeight
{
    UIViewController *vc = (UIViewController *)cellParent;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, vc.view.frame.size.width, self.frame.size.height);
    
    //float height = 0.0f;
    
    CGRect rect = self.lblMessage.frame;
    rect.origin.y = 8;
    rect.size.width = self.frame.size.width - (16*2);
    rect.size.height = CGFLOAT_MAX;
    self.lblMessage.frame = rect;
    
    [self.lblMessage sizeToFit];
    
    rect = self.lblMessage.frame;
    rect.origin.y = 8;
    rect.size.width = self.frame.size.width - (16*6+4);
    self.lblMessage.frame = rect;
    
    rect = self.bubbleView.frame;
    //rect.size.width = self.frame.size.width - (16*2);
    rect.size.width = self.lblMessage.frame.size.width+20;
    rect.size.height = self.lblMessage.frame.origin.y + self.lblMessage.frame.size.height + 8;
    self.bubbleView.frame = rect;
}

- (CGFloat)getHeight;
{
    float height = self.bubbleView.frame.size.height;
    return height;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
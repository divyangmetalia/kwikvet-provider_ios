//
//  FeedBackVC.m
//  UberNewDriver
//
//  Created by Elluminati on 27/09/14.
//  Copyright (c) 2014 Elluminati. All rights reserved.
//
#import "FeedBackVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "subTypeCell.h"

@interface FeedBackVC ()
{
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strRequsetId;
    
    NSString *strTime;
    NSString *strDistance;
    NSString *strProfilePic;
    NSString *strLastName;
    NSString *strFirstName;
    NSString *strForCurrency;
    
    NSMutableArray *arrSubTypes;
    float rate;
}

@end

@implementation FeedBackVC

@synthesize txtComment;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customFont];
    [self localizeString];
    
    strDistance=[PREF objectForKey:PREF_WALK_DISTANCE];
    strTime=[PREF objectForKey:PREF_WALK_TIME];
    strProfilePic=[PREF objectForKey:PREF_USER_PICTURE];
    strLastName=[PREF objectForKey:PREF_USER_NAME];

    NSArray *myWords = [strLastName componentsSeparatedByString:@" "];
    
    self.lblFirstName.text=[myWords objectAtIndex:0];
    self.lblLastName.text=[NSString stringWithFormat:@"%@ %@",[myWords objectAtIndex:0],[myWords objectAtIndex:1]];
    [self.imgProfile applyRoundedCornersFull];
    [self.imgProfile downloadFromURL:strProfilePic withPlaceholder:nil];
    
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];

    self.viewForBill.hidden=NO;
    [self setPriceValue];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"Invoice", nil) forState:UIControlStateNormal];
    
    arrSubTypes = [[NSMutableArray alloc]init];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"type"]];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"normal_order"]];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"remote_order"]];
    [arrSubTypes addObjectsFromArray:[dictBillInfo valueForKey:@"followup_order"]];
    
    strForCurrency = [dictBillInfo valueForKey:@"currency"];
    
    NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"PROMO BOUNCE", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"promo_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
    dictprice = [[NSMutableDictionary alloc]init];
    [dictprice setValue:NSLocalizedString(@"REFERRAL BOUNCE", nil) forKey:@"name"];
    [dictprice setValue:[dictBillInfo valueForKey:@"referral_bonus"] forKey:@"price"];
    [arrSubTypes addObject:dictprice];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [APPDELEGATE hideLoadingView];
    [self.btnMenu setTitle:NSLocalizedString(@"Invoice", nil) forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)giveFeedback
{
    strUserId=[PREF objectForKey:PREF_USER_ID];
    strUserToken=[PREF objectForKey:PREF_USER_TOKEN];
    strRequsetId=[PREF objectForKey:PREF_REQUEST_ID];
    
    if (strRequsetId!=nil)
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        
        [dictparam setObject:[NSString stringWithFormat:@"%f",rate] forKey:PARAM_RATING];
        
        NSString *commt=self.txtComment.text;
        if([commt isEqualToString:NSLocalizedString(@"COMMENTS", nil)])
        {
            [dictparam setObject:@"" forKey:PARAM_COMMENT];
        }
        else
        {
            [dictparam setObject:self.txtComment.text forKey:PARAM_COMMENT];
        }
        
        if([self.textViewDiagnostic.text isEqualToString:NSLocalizedString(@"DIAGNOSTIC_COMMENTS", nil)])
        {
            [dictparam setObject:@"" forKey:@"diagnostic_comment"];
        }
        else
        {
            [dictparam setObject:self.textViewDiagnostic.text forKey:@"diagnostic_comment"];
        }
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_RATING withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING_COMPLETED", nil)];
                 [PREF removeObjectForKey:PREF_REQUEST_ID];
                 [PREF removeObjectForKey:PREF_USER_NAME];
                 [PREF removeObjectForKey:PREF_USER_PHONE];
                 [PREF removeObjectForKey:PREF_USER_PICTURE];
                 [PREF removeObjectForKey:PREF_USER_RATING];
                 [PREF removeObjectForKey:PREF_START_TIME];
                 [PREF removeObjectForKey:PREF_DIAGNOSTIC_CODE];
                 [PREF synchronize];
                 is_completed=0;
                 is_dog_rated=0;
                 is_started=0;
                 is_walker_arrived=0;
                 is_walker_started=0;
                 [self.navigationController popToRootViewControllerAnimated:YES];
             }
             else
             {
                 NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                 [alert show];
             }
         }];
    }
}

#pragma mark -
#pragma mark - Custom Methods

-(void)customFont
{
    self.btnSubmit=[APPDELEGATE setBoldFontDiscriptor:self.btnSubmit];
    self.lblTotal.font=[UberStyleGuide fontRegularLight:42.13f];
    self.lblFirstName.font=[UberStyleGuide fontRegularLight:20.0f];
    self.lblLastName.font=[UberStyleGuide fontRegularLight:20.0f];
    self.lblTotalDue.font=[UberStyleGuide fontRegularLight];

    self.btnMenu.titleLabel.font=[UberStyleGuide fontRegularLight];
    self.btnMenu.titleLabel.textColor = [UberStyleGuide fontColorNevigation];
    
    self.textViewDiagnostic.textContainer.maximumNumberOfLines=8;
}

-(void)localizeString
{
    self.lblInvoice.text = NSLocalizedString(@"Invoice", nil);
    self.lblTotalDue.text = NSLocalizedString(@"Total Due", nil);
    self.lblComment.text = NSLocalizedString(@"COMMENT", nil);
    self.txtComment.text = NSLocalizedString(@"COMMENTS", nil);
    self.textViewDiagnostic.text = NSLocalizedString(@"DIAGNOSTIC_COMMENTS", nil);
    self.lblDiagnostic.text = NSLocalizedString(@"DIAGNOSTIC", nil);
    self.lblPaymentModeTitle.text = NSLocalizedString(@"Payment Mode", nil);
}

#pragma mark-
#pragma mark- Set Invoice Details

-(void)setPriceValue
{
    self.lblTotal.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"total"] floatValue]];
    
    if([[dictBillInfo valueForKey:@"payment_type"]intValue]==1)
    {
        self.lblPaymentMode.text = NSLocalizedString(@"Cash Payment", nil);
    }
    else
    {
        self.lblPaymentMode.text = NSLocalizedString(@"Card Payment", nil);
    }
}

#pragma mark-
#pragma mark- TableView Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrSubTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"subType";
    
    subTypeCell *cell = [self.tableForTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[subTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dict = [arrSubTypes objectAtIndex:indexPath.row];
    
    cell.lblTypeName.text = [dict valueForKey:@"name"];
    cell.lblPrice.text =[NSString stringWithFormat:@"%@ %.2f",strForCurrency,[[dict valueForKey:@"price"] floatValue]];
    
    return cell;
}

#pragma mark-
#pragma mark- Button Methods

- (IBAction)submitBtnPressed:(id)sender
{
    [self.txtComment resignFirstResponder];
    RBRatings rating=[ratingView getcurrentRatings];
    rate=rating/2.0;
    if (rating%2 != 0)
    {
        rate += 0.5;
    }
    if (rate==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLEASE_RATINGS", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:Nil, nil];
        [alert show];
    }
    else
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"WAITING_FOR_FEEDBACK", nil)];
        [txtComment resignFirstResponder];
        [self giveFeedback];
    }
}

- (IBAction)confirmBtnPressed:(id)sender
{
    self.viewForBill.hidden=YES;
    [self.btnMenu setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
    ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(150, 27) AndPosition:CGPointMake(85, 210)];
    ratingView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:ratingView];
}

#pragma mark-
#pragma mark- Text Field Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtComment resignFirstResponder];
    [self.textViewDiagnostic resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == self.txtComment)
        self.txtComment.text = @"";
    if(textView == self.textViewDiagnostic)
        self.textViewDiagnostic.text = @"";
    
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComment)
            {
                UITextPosition *beginning = [self.txtComment beginningOfDocument];
                [self.txtComment setSelectedTextRange:[self.txtComment textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
            if(textView == self.textViewDiagnostic)
            {
                UITextPosition *beginning = [self.textViewDiagnostic beginningOfDocument];
                [self.textViewDiagnostic setSelectedTextRange:[self.textViewDiagnostic textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -75, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComment)
            {
                UITextPosition *beginning = [self.txtComment beginningOfDocument];
                [self.txtComment setSelectedTextRange:[self.txtComment textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
            if(textView == self.textViewDiagnostic)
            {
                UITextPosition *beginning = [self.textViewDiagnostic beginningOfDocument];
                [self.textViewDiagnostic setSelectedTextRange:[self.textViewDiagnostic textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -75, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComment)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
            if(textView == self.textViewDiagnostic)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComment)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
            if(textView == self.textViewDiagnostic)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
    if ([txtComment.text isEqualToString:@""])
    {
        txtComment.text=NSLocalizedString(@"COMMENTS", nil);;
    }
    
    if ([self.textViewDiagnostic.text isEqualToString:@""])
    {
        self.textViewDiagnostic.text=NSLocalizedString(@"DIAGNOSTIC_COMMENTS", nil);;
    }
}
@end
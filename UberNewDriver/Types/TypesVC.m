//
//  TypesVC.m
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 9/2/16.
//  Copyright © 2016 Deep Gami. All rights reserved.

#import "TypesVC.h"
#import "TypesNewCell.h"

@interface TypesVC ()
{
    NSMutableArray *arrForTypes,*arrTypeID;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strRequsetId;
}

@end

@implementation TypesVC

@synthesize strTitle;

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrForTypes = [[NSMutableArray alloc] init];
    arrTypeID = [[NSMutableArray alloc] init];
    strUserId=[PREF objectForKey:PREF_USER_ID];
    strUserToken=[PREF objectForKey:PREF_USER_TOKEN];
    strRequsetId=[PREF objectForKey:PREF_REQUEST_ID];
    if(strRequsetId.length>0)
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        self.tableForTypes.contentInset = UIEdgeInsetsZero;
        self.automaticallyAdjustsScrollViewInsets = NO;
        [self setData:strTitle];
    }
    else
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_ON_TRIP", nil)];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    [self localization];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)localization
{
    [self.btnConfirmType setTitle:NSLocalizedString(@"CONFIRM_WITH_USER", nil) forState:UIControlStateNormal];
    [self.btnConfirmType setTitle:NSLocalizedString(@"CONFIRM_WITH_USER", nil) forState:UIControlStateHighlighted];
}

#pragma mark - Custom methods

-(void)setData:(NSString *)str
{
    if([str isEqualToString:@"2"])
    {
        [self.btnMenu setTitle:NSLocalizedString(@"NAV_BTN_TYPE", nil) forState:UIControlStateNormal];
        [self.btnMenu setTitle:NSLocalizedString(@"NAV_BTN_TYPE", nil) forState:UIControlStateHighlighted];
        
        [self getTypes:str];
    }
    else
    {
        [self.btnMenu setTitle:NSLocalizedString(@"NAV_BTN_REMOTE_TYPE", nil) forState:UIControlStateNormal];
        [self.btnMenu setTitle:NSLocalizedString(@"NAV_BTN_REMOTE_TYPE", nil) forState:UIControlStateHighlighted];
        self.btnMenu.frame = CGRectMake(self.btnMenu.frame.origin.x, self.btnMenu.frame.origin.y, 140, self.btnMenu.frame.size.height);
        
        [self getTypes:str];
    }
}

-(void)getTypes:(NSString *)str
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        
        NSString *strType;
        
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        
        if([strTitle isEqualToString:@"2"])
            strType = @"0";
        else
            strType = @"1";
        
        [dictparam setObject:strType forKey:@"type"];
        
        AFNHelper *afn = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        
        [afn getDataFromPath:FILE_REQUEST_TYPES withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     NSLog(@"Types = %@",response);
                     [arrForTypes addObjectsFromArray:[response valueForKey:@"types"]];
                     if(arrForTypes.count!=0)
                         [self.tableForTypes reloadData];
                     else
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_TYPES", nil)];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self.timerForGetrequest invalidate];
                         self.timerForGetrequest = nil;
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
             }
             
             [APPDELEGATE hideLoadingView];
             
             NSLog(@"RESPONSE --> %@",response);
         }];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)checkForGetrequest
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_GET_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Type Getrequest = %@",response);
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     NSMutableDictionary *dictRequest=[response valueForKey:@"request"];
                     
                     if([[dictRequest valueForKey:@"offer_flag"] intValue]==0)
                     {
                         [APPDELEGATE hideLoadingView];
                         [self.timerForGetrequest invalidate];
                         self.timerForGetrequest = nil;
                         if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==1)
                         {
                             NSLog(@"Provider type requets accepted");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"PROVIDER_TYPE_ACCEPTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                         else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==2)
                         {
                             NSLog(@"Provider type requets rejected");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"PROVIDER_TYPE_REJECTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                         else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==3)
                         {
                             NSLog(@"Remote type requets accepted");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"REMOTE_TYPE_ACCEPTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                         else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==4)
                         {
                             NSLog(@"Remote type requets rejected");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"REMOTE_TYPE_REJECTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                         else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==5)
                         {
                             NSLog(@"Follow up accepted");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"FOLLOWUP_TYPE_ACCEPTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                         else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==6)
                         {
                             NSLog(@"Follow up rejected");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"FOLLOWUP_TYPE_REJECTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                     }
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self.timerForGetrequest invalidate];
                         self.timerForGetrequest = nil;
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==222)
    {
        if([APPDELEGATE connected])
        {
            NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
            
            [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_WAIT", nil)];
            
            [dictparam setObject:strUserId forKey:PARAM_ID];
            [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
            [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CONFIRM_USER_RESPONSE withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"Confirm Responce = %@",response);
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [self.timerForGetrequest invalidate];
                         self.timerForGetrequest = nil;
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         if ([[response valueForKey:@"error_code"] integerValue]==406)
                         {
                             [self.timerForGetrequest invalidate];
                             self.timerForGetrequest = nil;
                             [self performSegueWithIdentifier:@"logout" sender:self];
                         }
                     }
                     
                     [APPDELEGATE hideLoadingView];
                 }
             }];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

#pragma mark - Tableview methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableForTypes registerNib:[UINib nibWithNibName:@"TypesNewCell" bundle:nil] forCellReuseIdentifier:@"types"];
    
    TypesNewCell *cell = [self.tableForTypes dequeueReusableCellWithIdentifier:@"types" forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[TypesNewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"types"];
    }
    
    NSMutableDictionary *dictForType = [[NSMutableDictionary alloc] init];
    
    dictForType = [arrForTypes objectAtIndex:indexPath.row];
    
    NSString *strCurrency = [dictForType valueForKey:@"currency"];
    
    cell.lblName.text = [dictForType valueForKey:@"name"];
    cell.lblPrice.text = [NSString stringWithFormat:@"%@ %@",strCurrency,[dictForType valueForKey:@"base_price"]];
    cell.lblBg.backgroundColor = [UIColor lightGrayColor];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TypesNewCell *cell=[self.tableForTypes cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.lblBg.backgroundColor = [UIColor darkGrayColor];
    NSString *strTypeid = [NSString stringWithFormat:@"%@",[[arrForTypes objectAtIndex:indexPath.row] valueForKey:@"id"]];
    [arrTypeID addObject:strTypeid];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TypesNewCell *cell=[self.tableForTypes cellForRowAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    cell.lblBg.backgroundColor = [UIColor lightGrayColor];
    NSString *strTypeid = [NSString stringWithFormat:@"%@",[[arrForTypes objectAtIndex:indexPath.row] valueForKey:@"id"]];
    [arrTypeID removeObject:strTypeid];
}

#pragma mark - Action Methods

- (IBAction)onClickMenu:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onClickConfirmType:(id)sender
{
    if(strRequsetId.length>1)
    {
        if(arrTypeID.count!=0)
        {
            if([APPDELEGATE connected])
            {
                [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                
                NSString *strType = [arrTypeID objectAtIndex:0];
                
                for(int i=1;i<arrTypeID.count;i++)
                {
                    strType = [strType stringByAppendingString:[NSString stringWithFormat:@",%@",[arrTypeID objectAtIndex:i]]];
                }
                
                NSMutableDictionary *dictParam = [[NSMutableDictionary alloc] init];
                [dictParam setObject:strUserId forKey:PARAM_ID];
                [dictParam setObject:strUserToken forKey:PARAM_TOKEN];
                [dictParam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
                [dictParam setObject:strType forKey:PARAM_TYPES];
                
                if([strTitle isEqualToString:@"2"])
                    [dictParam setObject:@"0" forKey:PARAM_OFFER_TYPE];
                else
                    [dictParam setObject:@"1" forKey:PARAM_OFFER_TYPE];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:FILE_CONFIRM_TYPES withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     if (response)
                     {
                         if([[response valueForKey:@"success"] intValue]==1)
                         {
                             NSLog(@"Confrim Types = %@",response);
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"WAIT_FOR_CONFIRMATION", nil)];
                             //[APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                             
                             NSRunLoop *runloop = [NSRunLoop currentRunLoop];
                             self.timerForGetrequest = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self selector:@selector(checkForGetrequest) userInfo:nil repeats:YES];
                             [runloop addTimer:self.timerForGetrequest forMode:NSRunLoopCommonModes];
                             [runloop addTimer:self.timerForGetrequest forMode:UITrackingRunLoopMode];
                             
                             //[self.navigationController popToRootViewControllerAnimated:YES];
                         }
                         else
                         {
                             if ([[response valueForKey:@"error_code"] integerValue]==406)
                             {
                                 [self.timerForGetrequest invalidate];
                                 self.timerForGetrequest = nil;
                                 [self performSegueWithIdentifier:@"logout" sender:self];
                             }
                             else
                             {
                                 NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                         }
                     }
                     
                     //[APPDELEGATE hideLoadingView];
                     
                     NSLog(@"RESPONSE --> %@",response);
                 }];
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_SELECT_TYPE", nil)];
    }
    else
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_ON_TRIP", nil)];
    }
}

@end
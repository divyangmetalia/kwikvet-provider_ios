//
//  TypesNewCell.h
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 9/12/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypesNewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblBg;

@end

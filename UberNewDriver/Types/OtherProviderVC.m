//  OtherProviderVc.m
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 9/2/16.
//  Copyright © 2016 Deep Gami. All rights reserved.

#import "OtherProviderVc.h"
#import "TypesNewCell.h"

@interface OtherProviderVC ()
{
    NSMutableArray *arrForOtherTypes,*arrOtherTypeID;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strRequsetId;
    NSIndexPath *selectedIndexPath;
}

@end

@implementation OtherProviderVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    arrForOtherTypes = [[NSMutableArray alloc] init];
    arrOtherTypeID = [[NSMutableArray alloc] init];
    strUserId=[PREF objectForKey:PREF_USER_ID];
    strUserToken=[PREF objectForKey:PREF_USER_TOKEN];
    strRequsetId=[PREF objectForKey:PREF_REQUEST_ID];
    
    self.tableForOtherTypes.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self localization];
    [self getProviders];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)localization
{
    [self.btnMenu setTitle:NSLocalizedString(@"NAV_BTN_OTHER_PROVDER", nil) forState:UIControlStateNormal];
    [self.btnMenu setTitle:NSLocalizedString(@"NAV_BTN_OTHER_PROVDER", nil) forState:UIControlStateHighlighted];
    [self.btnConirmOtherTypes setTitle:NSLocalizedString(@"CONFIRM_WITH_USER", nil) forState:UIControlStateNormal];
    [self.btnConirmOtherTypes setTitle:NSLocalizedString(@"CONFIRM_WITH_USER", nil) forState:UIControlStateHighlighted];
}

#pragma mark - Custom methods

-(void)getProviders
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
    
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam;
        dictparam= [[NSMutableDictionary alloc]init];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_OTHER_PROVIDER_TYPES withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                      NSLog(@"Types = %@",response);
                     [arrForOtherTypes addObjectsFromArray:[response valueForKey:@"types"]];
                     if(arrForOtherTypes.count!=0)
                         [self.tableForOtherTypes reloadData];
                     else
                     {
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_TYPES", nil)];
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self.timerForProviderGetrequest invalidate];
                         self.timerForProviderGetrequest = nil;
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
             }
             
             [APPDELEGATE hideLoadingView];
             
             NSLog(@"RESPONSE --> %@",response);
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)checkForProviderGetrequest
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_GET_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Type Getrequest = %@",response);
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     NSMutableDictionary *dictRequest=[response valueForKey:@"request"];
                     
                     if([[dictRequest valueForKey:@"offer_flag"] intValue]==0)
                     {
                         [APPDELEGATE hideLoadingView];
                         [self.timerForProviderGetrequest invalidate];
                         self.timerForProviderGetrequest = nil;
                         if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==5)
                         {
                             NSLog(@"Follow up accepted");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"FOLLOWUP_TYPE_ACCEPTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                         else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==6)
                         {
                             NSLog(@"Follow up rejected");
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"FOLLOWUP_TYPE_REJECTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             alert.tag=222;
                             [alert show];
                         }
                     }
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self.timerForProviderGetrequest invalidate];
                         self.timerForProviderGetrequest = nil;
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==222)
    {
        if([APPDELEGATE connected])
        {
            NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
            
            [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_WAIT", nil)];

            [dictparam setObject:strUserId forKey:PARAM_ID];
            [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
            [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CONFIRM_USER_RESPONSE withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 NSLog(@"Confirm Responce = %@",response);
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [self.timerForProviderGetrequest invalidate];
                         self.timerForProviderGetrequest = nil;
                         [self.navigationController popToRootViewControllerAnimated:YES];
                     }
                     else
                     {
                         if ([[response valueForKey:@"error_code"] integerValue]==406)
                         {
                             [self.timerForProviderGetrequest invalidate];
                             self.timerForProviderGetrequest = nil;
                             [self performSegueWithIdentifier:@"logout" sender:self];
                         }
                     }
                     [APPDELEGATE hideLoadingView];
                 }
             }];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
}

#pragma mark - Tableview methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForOtherTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableForOtherTypes registerNib:[UINib nibWithNibName:@"TypesNewCell" bundle:nil] forCellReuseIdentifier:@"types"];
    
    TypesNewCell *cell = [self.tableForOtherTypes dequeueReusableCellWithIdentifier:@"types" forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[TypesNewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"types"];
    }
    
    NSMutableDictionary *dictForType = [[NSMutableDictionary alloc] init];
    
    dictForType = [arrForOtherTypes objectAtIndex:indexPath.row];
    
    NSString *strCurrency = [dictForType valueForKey:@"currency"];
    
    cell.lblName.text = [dictForType valueForKey:@"name"];
    cell.lblPrice.text = [NSString stringWithFormat:@"%@ %@",strCurrency,[dictForType valueForKey:@"base_price"]];
    cell.lblBg.backgroundColor = [UIColor lightGrayColor];
    
    if(selectedIndexPath.length>0)
    {
        if([selectedIndexPath isEqual:indexPath])
        {
            cell.backgroundColor = [UIColor lightGrayColor];
            cell.lblBg.backgroundColor = [UIColor colorWithRed:170.0f/255.0f green:170.0f/255.0f blue:170.0f/255.0f alpha:0];
            NSString *strTypeid = [NSString stringWithFormat:@"%@",[[arrForOtherTypes objectAtIndex:indexPath.row] valueForKey:@"id"]];
            [arrOtherTypeID removeAllObjects];
            [arrOtherTypeID addObject:strTypeid];
        }
        else
        {
            cell.backgroundColor = [UIColor clearColor];
            cell.lblBg.backgroundColor = [UIColor lightGrayColor];
            //[arrOtherTypeID removeAllObjects];
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndexPath = indexPath;
    
    [self.tableForOtherTypes reloadData];
}

#pragma mark - Action Methods

- (IBAction)onClickMenu:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)onClickConfirmOtherTypes:(id)sender
{
    if(strRequsetId.length>1)
    {
        if(arrOtherTypeID.count!=0)
        {
            if([APPDELEGATE connected])
            {
                [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                
                NSString *strType = [arrOtherTypeID objectAtIndex:0];
                
                for(int i=1;i<arrOtherTypeID.count;i++)
                {
                    strType = [strType stringByAppendingString:[NSString stringWithFormat:@",%@",[arrOtherTypeID objectAtIndex:i]]];
                }
     
                NSMutableDictionary *dictParam = [[NSMutableDictionary alloc] init];
                [dictParam setObject:strUserId forKey:PARAM_ID];
                [dictParam setObject:strUserToken forKey:PARAM_TOKEN];
                [dictParam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
                [dictParam setObject:strType forKey:PARAM_TYPES];
                [dictParam setObject:@"2" forKey:PARAM_OFFER_TYPE];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:FILE_CONFIRM_TYPES withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     if (response)
                     {
                         if([[response valueForKey:@"success"] intValue]==1)
                         {
                             NSLog(@"Confrim Types = %@",response);
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"WAIT_FOR_CONFIRMATION", nil)];
                             NSRunLoop *runloop = [NSRunLoop currentRunLoop];
                             self.timerForProviderGetrequest = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self selector:@selector(checkForProviderGetrequest) userInfo:nil repeats:YES];
                             [runloop addTimer:self.timerForProviderGetrequest forMode:NSRunLoopCommonModes];
                             [runloop addTimer:self.timerForProviderGetrequest forMode:UITrackingRunLoopMode];
                         }
                         else
                         {
                             if ([[response valueForKey:@"error_code"] integerValue]==406)
                             {
                                 [self.timerForProviderGetrequest invalidate];
                                 self.timerForProviderGetrequest = nil;
                                 [self performSegueWithIdentifier:@"logout" sender:self];
                             }
                             else
                             {
                                 NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                         }
                     }
                     NSLog(@"RESPONSE --> %@",response);
                 }];
            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_SELECT_TYPE", nil)];
    }
    else
    {
        [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_ON_TRIP", nil)];
    }
}
@end

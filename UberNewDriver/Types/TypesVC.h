//
//  TypesVC.h
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 9/2/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import "BaseVC.h"
#import "TypesNewCell.h"


@interface TypesVC : BaseVC<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirmType;
@property (weak, nonatomic) IBOutlet UITableView *tableForTypes;

@property(nonatomic, strong) NSTimer *timerForGetrequest;

@property (strong , nonatomic) NSString *strTitle;

- (IBAction)onClickMenu:(id)sender;
- (IBAction)onClickConfirmType:(id)sender;

@end

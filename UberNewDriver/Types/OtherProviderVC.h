//
//  OtherProviderVc.h
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 9/2/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import "BaseVC.h"

@interface OtherProviderVC : BaseVC

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UITableView *tableForOtherTypes;
@property (weak, nonatomic) IBOutlet UIButton *btnConirmOtherTypes;

@property(nonatomic, strong) NSTimer *timerForProviderGetrequest;

- (IBAction)onClickMenu:(id)sender;
- (IBAction)onClickConfirmOtherTypes:(id)sender;

@end

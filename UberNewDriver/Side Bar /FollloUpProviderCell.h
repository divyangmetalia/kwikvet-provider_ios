//
//  FollloUpProviderCell.h
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 11/25/16.
//  Copyright © 2016 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollloUpProviderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgFollowUp;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@end

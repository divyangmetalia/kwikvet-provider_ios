//
//  HistoryDetailVC.m
//  Rider Driver
//
//  Created by Raj Oriya on 7/8/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import "HistoryDetailVC.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "CollectionCell.h"
#import "ChatVC.h"
#import "FollloUpProviderCell.h"

@interface HistoryDetailVC ()
{
    NSMutableArray *arrForLabResult,*arrForScanDocuments,*arrForFollowUpProviders;
    NSInteger tag;
    BOOL isLab;
    NSString *strClient,*strFollowUpProviderid;
}

@end

@implementation HistoryDetailVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOADING_HISTORY", nil)];
    self.navigationItem.hidesBackButton = YES;
    
    NSLog(@"%@",self.dictInfo);
    [self customeSetup];
    [self setLocalization];
    [self setTripDetails];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.viewForlLagreImage addGestureRecognizer:swipeLeft];
    [self.viewForlLagreImage addGestureRecognizer:swipeRight];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - custom method

-(void)handleSwipe:(UISwipeGestureRecognizer *)sender
{
    
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *)sender direction];
    
    switch (direction)
    {
        case UISwipeGestureRecognizerDirectionLeft:
            tag++;
            break;
        case UISwipeGestureRecognizerDirectionRight:
            tag--;
            break;
        default:
            break;
    }
    
    if(isLab==0)
    {
        tag = (tag < 0 )? ([arrForLabResult count] -1):
        tag % [arrForLabResult count];
        
        [self.imgFullImage downloadFromURL:[[arrForLabResult objectAtIndex:tag] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    }
    else
    {
        tag = (tag < 0 )? ([arrForScanDocuments count] -1):
        tag % [arrForScanDocuments count];
        
        [self.imgFullImage downloadFromURL:[[arrForScanDocuments objectAtIndex:tag] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    }
}

-(void)customeSetup
{
    self.viewForlLagreImage.hidden=YES;
    
    arrForLabResult = [[NSMutableArray alloc] init];
    arrForScanDocuments = [[NSMutableArray alloc] init];
    arrForFollowUpProviders = [[NSMutableArray alloc] init];
    
    self.noItemsForLab.hidden=YES;
    self.noItemsForScan.hidden=YES;
    self.viewForFollowUpProvider.hidden=YES;
    
    self.btnChatWithClient.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.btnChatWithProvider.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    [self getFollowUpProviders];
}

-(void)setLocalization
{
    self.lblDiagnosticComment.text = NSLocalizedString(@"DIAGNOSTIC_COMMENT", nil);
    self.lblLabResult.text = NSLocalizedString(@"LAB_RESULT", nil);
    self.lblScanDocuments.text = NSLocalizedString(@"SCAN_DOCUMENT", nil);
    self.lblSelectFollowUpProvider.text = NSLocalizedString(@"SELECT_PROVIDER", nil);
    
    [self.btnBack setTitle:NSLocalizedString(@"History", nil) forState:UIControlStateNormal];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnChatWithClient setTitle:NSLocalizedString(@"CHAT_CLIENT", nil) forState:UIControlStateNormal];
    [self.btnChatWithProvider setTitle:NSLocalizedString(@"CHAT_PROVIDER", nil) forState:UIControlStateNormal];
}

-(void)setTripDetails
{
    NSString *strComment = [self.dictInfo valueForKey:@"diagnostic_comment"];
    
    if(strComment.length>0)
        self.textViewComment.text = [self.dictInfo valueForKey:@"diagnostic_comment"];
    else
        self.textViewComment.text = NSLocalizedString(@"NOT_AVAILABLE", nil);
    
    [arrForLabResult addObjectsFromArray:[self.dictInfo valueForKey:@"lab_result"]];
    [arrForScanDocuments addObjectsFromArray:[self.dictInfo valueForKey:@"scan_document"]];
    
    if(arrForLabResult.count>0)
        [self.collectionViewForLab reloadData];
    else
        self.noItemsForLab.hidden=NO;
    
    if(arrForScanDocuments.count>0)
        [self.collectionViewForScan reloadData];
    else
        self.noItemsForScan.hidden=NO;
    
    [APPDELEGATE hideLoadingView];
}

-(void)getFollowUpProviders
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc] init];
        
        [dictParam setValue:[NSString stringWithFormat:@"%@",[self.dictInfo valueForKey:@"id"]] forKey:PARAM_REQUEST_ID];
        [dictParam setValue:[PREF objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setValue:[PREF objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *afn = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_FOLLOW_UP_PROVIDER withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [arrForFollowUpProviders removeAllObjects];
                     [arrForFollowUpProviders addObjectsFromArray:[response valueForKey:@"data"]];
                     [self.tableForFolloUpProvider reloadData];
                 }
                 else
                 {
                     
                 }
             }
             [APPDELEGATE hideLoadingView];
         }];
    }
}

#pragma mark -
#pragma mark - ColetionView Methods

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == self.collectionViewForLab)
        return  arrForLabResult.count;
    else
        return  arrForScanDocuments.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionCell *cell = [self.collectionViewForLab dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    
    if(collectionView == self.collectionViewForLab)
        [cell.imgForLab downloadFromURL:[[arrForLabResult objectAtIndex:indexPath.row] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    
    else
        [cell.imgForLab downloadFromURL:[[arrForScanDocuments objectAtIndex:indexPath.row] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == self.collectionViewForLab)
    {
        isLab=0;
        [self.imgFullImage downloadFromURL:[[arrForLabResult objectAtIndex:indexPath.row] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    }
    else
    {
        isLab=1;
        [self.imgFullImage downloadFromURL:[[arrForScanDocuments objectAtIndex:indexPath.row] valueForKey:@"document"] withPlaceholder:[UIImage imageNamed:@"user"]];
    }
    
    tag = indexPath.row;
    self.viewForlLagreImage.hidden=NO;
}

#pragma mark - 
#pragma mark - Table View Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return arrForFollowUpProviders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"segueToFollowUp";
    
    FollloUpProviderCell *cell = [self.tableForFolloUpProvider dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[FollloUpProviderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSMutableDictionary *dictWalker=[arrForFollowUpProviders objectAtIndex:indexPath.row];
    
    cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[dictWalker valueForKey:@"first_name"],[dictWalker valueForKey:@"last_name"]];
    
    NSString *imagePath = [NSString stringWithFormat:@"%@",[dictWalker valueForKey:@"picture"]];
    
    [cell.imgFollowUp downloadFromURL:imagePath withPlaceholder:[UIImage imageNamed:@"user"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    strFollowUpProviderid = [[NSString stringWithFormat:@"%@",[arrForFollowUpProviders objectAtIndex:indexPath.row]]valueForKey:@"id"];
    [self performSegueWithIdentifier:@"segueToChat" sender:self];
}

#pragma mark -
#pragma mark - Action Methods

- (IBAction)onClickCloseView:(id)sender
{
    self.viewForlLagreImage.hidden=YES;
}

- (IBAction)btnBackPressed:(id)sender
{
    if(self.viewForFollowUpProvider.hidden==NO)
        self.viewForFollowUpProvider.hidden=YES;
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickChat:(id)sender
{
    if(sender == self.btnChatWithProvider)
    {
        strClient=@"0";
        self.viewForFollowUpProvider.hidden=NO;
        if(arrForFollowUpProviders.count==0)
        {
            self.tableForFolloUpProvider.hidden=YES;
            self.noItemsForProvider.hidden=NO;
        }
        else
        {
            self.tableForFolloUpProvider.hidden=NO;
            self.noItemsForProvider.hidden=YES;
        }
    }
    else
    {
        strClient=@"1";
        [self performSegueWithIdentifier:@"segueToChat" sender:self];
    }
}

#pragma  mark -
#pragma  mark -  prepare for segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"segueToChat"])
    {
        ChatVC *vc = [segue destinationViewController];
        vc.dictHistoryInfo = self.dictInfo;
        vc.strClient = strClient;
        vc.strFollowUpProviderId = strFollowUpProviderid;
    }
}

@end
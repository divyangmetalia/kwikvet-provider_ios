//
//  HistoryDetailVC.h
//  Rider Driver
//
//  Created by Raj Oriya on 7/8/15.
//  Copyright (c) 2015 Elluminati. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"

@interface HistoryDetailVC : BaseVC<UIAlertViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSString *strID;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgFullImage;

@property (weak, nonatomic) IBOutlet UIView *viewForlLagreImage;

@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewForLab;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewForScan;
@property (weak, nonatomic) IBOutlet UIButton *btnChatWithClient;
@property (weak, nonatomic) IBOutlet UIButton *btnChatWithProvider;

- (IBAction)onClickChat:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *viewForFollowUpProvider;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectFollowUpProvider;
@property (weak, nonatomic) IBOutlet UITableView *tableForFolloUpProvider;

@property (strong,nonatomic) NSDictionary *dictInfo;
@property (strong,nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblDiagnosticComment;
@property (weak, nonatomic) IBOutlet UILabel *lblLabResult;
@property (weak, nonatomic) IBOutlet UILabel *lblScanDocuments;
@property (weak, nonatomic) IBOutlet UIImageView *noItemsForLab;
@property (weak, nonatomic) IBOutlet UIImageView *noItemsForScan;
@property (weak, nonatomic) IBOutlet UIImageView *noItemsForProvider;

@property (weak, nonatomic) IBOutlet UITextView *textViewComment;
- (IBAction)onClickCloseView:(id)sender;

- (IBAction)btnBackPressed:(id)sender;
@end
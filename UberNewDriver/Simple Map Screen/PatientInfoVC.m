//  PatientInfoVC.m
//  KwikVet for VETS
//  Created by Elluminati Macbook Pro 2 on 9/30/16.
//  Copyright © 2016 Deep Gami. All rights reserved.

#import "PatientInfoVC.h"
#import "SWRevealViewController.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "TypesCell.h"

@interface PatientInfoVC ()
{
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strRequsetId;
    NSString *strBirthDateOfPet,*strPetDetailsId;
    NSMutableArray *arrForDiagnositcLists,*arrForSelected,*arrForNormalTypes,*arrForRemoteTypes,*arrTypeID,*arrForFollowUpProvider;
    NSArray *arrForDog,*arrForCat;
    BOOL isDiagnostic,isFormFilled;
    int isType,isFirstLoad,isCat;
    CGRect frameForConfirmButton,frameForBackButton,frameForSkipButton;
}

@end

@implementation PatientInfoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self cusotmSetup];
    [self localization];
    [self getRequest];
    [self getDiagnosticList];
    [self getFolloUpProviders];
    // Do any additional setup after loading the view.
}

#pragma mark
#pragma mark - custom methods

-(void)localization
{
    [self.btnMenu setTitle:NSLocalizedString(@"APPNAME", nil) forState:UIControlStateNormal];
    [self.btnJob setTitle:NSLocalizedString(@"END TRIP",nil) forState:UIControlStateNormal];
    [self.btnJob setTitle:NSLocalizedString(@"END TRIP",nil) forState:UIControlStateSelected];
    [self.btnJob setTitle:NSLocalizedString(@"END TRIP",nil) forState:UIControlStateHighlighted];
    [self.btnDonePicker setTitle:NSLocalizedString(@"DONE", nil) forState:UIControlStateNormal];
    [self.btnDoneDetails setTitle:NSLocalizedString(@"DONE", nil) forState:UIControlStateNormal];
    [self.btnDiagnosticList setTitle:NSLocalizedString(@"DONE", nil) forState:UIControlStateNormal];
    [self.btnConfirm setTitle:NSLocalizedString(@"CONFIRM_WITH_USER", nil) forState:UIControlStateNormal];
    [self.btnSkip setTitle:NSLocalizedString(@"GO_TO_REMOTE", nil) forState:UIControlStateNormal];
    [self.btnBack setTitle:NSLocalizedString(@"GO_TO_REMOTE", nil) forState:UIControlStateNormal];
    
    self.txtPetName.placeholder = NSLocalizedString(@"ENTER_PET_NAME", nil);
    self.txtPetDob.placeholder = NSLocalizedString(@"ENTER_PET_DOB", nil);
    self.txtPetWeight.placeholder = NSLocalizedString(@"ENTER_PET_WEIGHT", nil);
    self.txtFormerVet.placeholder = NSLocalizedString(@"ENTER_PET_FORMER_VET", nil);
    self.txtPetBreed.placeholder = NSLocalizedString(@"ENTER_PET_BREED", nil);
    
    self.lblDiagnosticList.text = NSLocalizedString(@"DIAGNOSTIC_LIST", nil);
    self.lblPatientInfo.text = NSLocalizedString(@"PATIENT_INFO", nil);
    
    strPetDetailsId=@"";
}

-(void)cusotmSetup
{
    strUserId=[PREF objectForKey:PREF_USER_ID];
    strUserToken=[PREF objectForKey:PREF_USER_TOKEN];
    strRequsetId=[PREF objectForKey:PREF_REQUEST_ID];
    
    arrForDog = [NSArray arrayWithObjects:@"American Eskim",@"Bassett Hound",@"Beagle",@"Bichon Frise",@"Boston Terrier",@"Boxer",@"Bulldog",@"Chihuahua",@"Chow Chow",@"Cocker Spaniel",@"Dachshund",@"Fox Terrier",@"German Shepherd",@"Golden Retriever",@"Jack Russell",@"Labrador Retriever",@"Lhaso Apso",@"Maltese",@"Miniature Pinscher",@"Miniature Schnauzer",@"Mixed Breed Large",@"Mixed Breed Medium",@"Mixed Breed Small",@"Pit Bull",@"Pomeranian",@"Pug",@"Rottweiler",@"Shar Pei",@"Shih Tzu",@"Standard Poodle",@"Toy Poodle",@"Yorkshire Terrier",nil];
    
    arrForCat = [NSArray arrayWithObjects:@"Abyssinian Cat",@"Birman Cat",@"Domestic Cat",@"Himalayan Cat",@"Maine Coon Cat",@"Manx Cat",@"Norwegian Forest Cat",@"Persian Cat",@"Ragdoll Cat",@"Scottish Fold Cat",@"Siamese Cat",nil];
    
    arrForNormalTypes = [[NSMutableArray alloc] init];
    arrForSelected = [[NSMutableArray alloc] init];
    arrForDiagnositcLists = [[NSMutableArray alloc] init];
    arrForRemoteTypes = [[NSMutableArray alloc] init];
    arrForFollowUpProvider = [[NSMutableArray alloc] init];
    arrTypeID = [[NSMutableArray alloc] init];
    
    self.viewForPatientInfo.hidden=NO;
    self.viewForDiagnosticList.hidden=YES;
    self.viewForPicker.hidden=YES;
    self.btnJob.hidden=YES;
    self.btnConfirm.hidden=YES;
    self.btnSkip.hidden=YES;
    self.btnBack.hidden=YES;
    self.imgNoItems.hidden=YES;
    self.viewForBreeds.hidden=YES;
    
    [self.btnBack.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.btnSkip.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.btnConfirm.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    frameForConfirmButton = self.btnConfirm.frame;
    frameForBackButton = self.btnBack.frame;
    frameForSkipButton = self.btnSkip.frame;
    
    isDiagnostic = YES;
    isType = 1;
    isFirstLoad = 0;
    
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
    
    self.tableForDaignosticList.frame = CGRectMake(self.tableForDaignosticList.frame.origin.x, self.tableForDaignosticList.frame.origin.y, self.tableForDaignosticList.frame.size.width,self.btnDiagnosticList.frame.origin.y);
    
    self.tableForDaignosticList.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableForBreeds.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
}
-(void)setPetDetails:(NSMutableDictionary *)dictPetDetails
{
    strPetDetailsId = [dictPetDetails valueForKey:@"pet_detail_id"];
    strBirthDateOfPet = [self DateConverter:[dictPetDetails valueForKey:@"dob"]];
    self.txtPetName.text = [dictPetDetails valueForKey:@"user_name"];
    self.txtPetWeight.text = [dictPetDetails valueForKey:@"weight"];
    self.txtFormerVet.text = [dictPetDetails valueForKey:@"former_vet"];
    [self.imgForPet downloadFromURL:[dictPetDetails valueForKey:@"photo"] withPlaceholder:[UIImage imageNamed:@"user"]];
    
    NSString *strBreed = [NSString stringWithFormat:@"%@",[dictPetDetails valueForKey:@"breed"]];
    
    if(strBreed.length>0)
        self.txtPetBreed.text = strBreed;
    else
        self.txtPetBreed.text = @"";
    
    [self.imgForPet applyRoundedCornersFull];
    
    self.txtPetDob.text = strBirthDateOfPet;
}

-(void)getDiagnosticList
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_DIAGNOSTIC_LIST withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 NSLog(@"Diagnosic List : %@",response);
                 
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [arrForDiagnositcLists removeAllObjects];
                     [arrForDiagnositcLists addObjectsFromArray:[response valueForKey:@"diagnostic"]];
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                 }
                 
                 [arrForSelected removeAllObjects];
                 [self.tableForDaignosticList reloadData];
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getRequest
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_GET_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Get Request = %@",response);
             if (response)
             {
                 //[APPDELEGATE hideLoadingView];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     NSMutableDictionary *dictRequest=[response valueForKey:@"request"];
                     
                     is_completed=[[dictRequest valueForKey:@"is_completed"]intValue];
                     is_dog_rated=[[dictRequest valueForKey:@"is_dog_rated"]intValue];
                     isCat=[[dictRequest valueForKey:@"is_cat"] intValue];
                     dictBillInfo=[dictRequest valueForKey:@"bill"];
                     
                     isFormFilled = [[dictRequest valueForKey:@"is_diagnostic"] intValue];
                     
                     if(is_completed)
                         [self performSegueWithIdentifier:@"jobToFeedback" sender:self];
                     
                     else
                     {
                         if(isFirstLoad==0)
                         {
                             isFirstLoad=1;
                             NSMutableArray *arrForPet = [[NSMutableArray alloc] init];
                             arrForPet = [response valueForKey:@"pet_details"];
                             
                             if(arrForPet.count>0)
                                 [self setPetDetails:[response valueForKey:@"pet_details"]];
                             
                             NSString *strCode = [dictRequest valueForKey:@"dignostic_code"];
                             
                             if(![strCode isEqualToString:@""])
                             {
                                 [PREF setValue:strCode forKey:PREF_DIAGNOSTIC_CODE];
                                 [PREF synchronize];
                                 if(isType!=3)
                                 {
                                     [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                                     [self getTypes:strCode];
                                 }
                                 else
                                 {
                                     self.viewForPatientInfo.hidden=YES;
                                     self.viewForDiagnosticList.hidden=NO;
                                 }
                             }
                         }
                     
                         if([[dictRequest valueForKey:@"offer_flag"] intValue]==0 && [[dictRequest valueForKey:@"offer_accept_flag"] integerValue]!=0)
                         {
                             [self.timerForGetRequest invalidate];
                             self.timerForGetRequest = nil;
                             
                             if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==1)
                             {
                                 NSLog(@"Provider type requets accepted");
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"PROVIDER_TYPE_ACCEPTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 alert.tag=222;
                                 [alert show];
                             }
                             else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==2)
                             {
                                 NSLog(@"Provider type requets rejected");
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"PROVIDER_TYPE_REJECTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 alert.tag=222;
                                 [alert show];
                             }
                             else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==3)
                             {
                                 NSLog(@"Remote type requets accepted");
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"REMOTE_TYPE_ACCEPTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 alert.tag=222;
                                 [alert show];
                             }
                             else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==4)
                             {
                                 NSLog(@"Remote type requets rejected");
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"REMOTE_TYPE_REJECTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 alert.tag=222;
                                 [alert show];
                             }
                             else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==5)
                             {
                                 NSLog(@"Follow up accepted");
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"FOLLOWUP_TYPE_ACCEPTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 alert.tag=222;
                                 [alert show];
                                 
                                 self.btnConfirm.hidden=YES;
                             }
                             else if([[dictRequest valueForKey:@"offer_accept_flag"] integerValue]==6)
                             {
                                 NSLog(@"Follow up rejected");
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"FOLLOWUP_TYPE_REJECTED", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 alert.tag=222;
                                 [alert show];
                             }
                             
                             [APPDELEGATE hideLoadingView];
                         }
                         else if([[dictRequest valueForKey:@"offer_flag"] intValue]!=0)
                         {
                             [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                         }
                         else
                         {
                             [APPDELEGATE hideLoadingView];
                         }
                     }
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                     else
                     {
                         
                     }
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)getTypes:(NSString *)str
{
    if([APPDELEGATE connected])
    {
        //[APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:str forKey:@"code"];
        [dict setValue:strRequsetId forKey:PARAM_REQUEST_ID];
        [dict setValue:strUserId forKey:PARAM_ID];
        [dict setValue:strUserToken forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_REQUEST_TYPES withParamData:dict withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 NSLog(@"Types = %@",response);
                 
                 if([[response valueForKey:@"success"] integerValue]==1)
                 {
                     isDiagnostic=NO;
                     self.btnJob.hidden=YES;
                     self.btnDiagnosticList.hidden=YES;
                     
                     self.lblDiagnosticList.text = NSLocalizedString(@"NORMAL_TYPES", nil);
                     
                     [arrForRemoteTypes removeAllObjects];
                     [arrForNormalTypes removeAllObjects];
                     
                     NSMutableArray *arr = [[NSMutableArray alloc]initWithArray:[response valueForKey:@"types"]];
                     
                     for(NSMutableDictionary *dict in arr)
                     {
                         if([[dict valueForKey:@"is_remote"] intValue]==1)
                            [arrForRemoteTypes addObject:dict];
                         else
                            [arrForNormalTypes addObject:dict];
                     }
                     
                     [self.btnBack setTitle:NSLocalizedString(@"GOT_TO_DIAGNOSTIC", nil) forState:UIControlStateNormal];
                     
                     self.btnConfirm.hidden=NO;
                     self.btnSkip.hidden=NO;
                     self.btnBack.hidden=NO;
                     self.btnConfirm.frame = CGRectMake(self.btnBack.frame.size.width+self.btnBack.frame.origin.x+1, self.btnConfirm.frame.origin.y, self.view.frame.size.width/3-1, self.btnConfirm.frame.size.height);
                     self.btnSkip.frame = CGRectMake(self.btnConfirm.frame.size.width+self.btnConfirm.frame.origin.x+1, self.btnSkip.frame.origin.y, self.view.frame.size.width/3, self.btnSkip.frame.size.height);
                     
                     if(arrForNormalTypes.count!=0)
                     {
                         self.tableForDaignosticList.frame = CGRectMake(self.tableForDaignosticList.frame.origin.x, self.tableForDaignosticList.frame.origin.y, self.tableForDaignosticList.frame.size.width,self.tableForDaignosticList.frame.size.height-20-2*(self.btnConfirm.frame.size.height));
                         
                         [self.tableForDaignosticList reloadData];
                     }
                     else
                     {
                         self.imgNoItems.hidden=NO;
                         [self.tableForDaignosticList setHidden:YES];
                     }
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                 }
             }
             
             self.viewForPatientInfo.hidden=YES;
             self.viewForDiagnosticList.hidden=NO;
             [APPDELEGATE hideLoadingView];
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)getFolloUpProviders
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam;
        dictparam= [[NSMutableDictionary alloc]init];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_OTHER_PROVIDER_TYPES withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     NSLog(@"Follow Up Providers = %@",response);
                     [arrForFollowUpProvider removeAllObjects];
                     [arrForFollowUpProvider addObjectsFromArray:[response valueForKey:@"types"]];
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                     else
                     {
                         NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)ConfirmUserResponse
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_CONFIRM_USER_RESPONSE withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Confirm Responce = %@",response);
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [self setTableData:0];
                     [self.tableForDaignosticList reloadData];
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self.timerForGetRequest invalidate];
                         self.timerForGetRequest = nil;
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                 }
                 
                 [APPDELEGATE hideLoadingView];
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)setTableData:(BOOL)moveBack
{
    if(moveBack==0)
    {
        if(isType==2)
        {
            self.lblDiagnosticList.text = NSLocalizedString(@"FOLLOW_UP", nil);
            
            [self.btnBack setTitle:NSLocalizedString(@"GO_TO_REMOTE", nil) forState:UIControlStateNormal];
            
            self.btnSkip.hidden=YES;
            self.btnJob.hidden=NO;
            self.btnConfirm.frame = CGRectMake(self.view.frame.size.width/2, self.btnJob.frame.origin.y-self.btnJob.frame.size.height-1,self.view.frame.size.width/2, self.btnConfirm.frame.size.height);
            self.btnBack.frame = CGRectMake(self.btnBack.frame.origin.x, self.btnJob.frame.origin.y-self.btnJob.frame.size.height-1,self.view.frame.size.width/2-1, self.btnBack.frame.size.height);
            
            if(arrForFollowUpProvider.count!=0)
            {
                isType=3;
                self.tableForDaignosticList.hidden=NO;
                self.imgNoItems.hidden=YES;
                [self.tableForDaignosticList reloadData];
            }
            else
            {
                self.tableForDaignosticList.hidden=YES;
                self.imgNoItems.hidden=NO;
            }
        }
        else if(isType==1)
        {
            self.lblDiagnosticList.text = NSLocalizedString(@"REMOTE_TYPES", nil);
            
            [self.btnSkip setTitle:NSLocalizedString(@"GO_TO_FOLLOW_UP", nil) forState:UIControlStateNormal];
            [self.btnBack setTitle:NSLocalizedString(@"GO_TO_NORMAL", nil) forState:UIControlStateNormal];
            
            self.btnBack.hidden=NO;
            self.btnConfirm.frame = CGRectMake(self.btnBack.frame.size.width+self.btnBack.frame.origin.x+1, self.btnConfirm.frame.origin.y, self.view.frame.size.width/3-1, self.btnConfirm.frame.size.height);
            self.btnSkip.frame = CGRectMake(self.btnConfirm.frame.size.width+self.btnConfirm.frame.origin.x+1, self.btnSkip.frame.origin.y, self.view.frame.size.width/3, self.btnSkip.frame.size.height);
            
            isType=2;
            
            if(arrForRemoteTypes.count!=0)
            {
                self.tableForDaignosticList.hidden=NO;
                self.imgNoItems.hidden=YES;
                [self.tableForDaignosticList reloadData];
            }
            else
            {
                self.tableForDaignosticList.hidden=YES;
                self.imgNoItems.hidden=NO;
            }
        }
        else
        {
            self.lblDiagnosticList.text = NSLocalizedString(@"FOLLOW_UP", nil);
            self.btnSkip.hidden=YES;
            self.btnJob.hidden=NO;
            self.btnConfirm.frame = CGRectMake(self.btnConfirm.frame.origin.x, self.btnJob.frame.origin.y-self.btnJob.frame.size.height-2,self.btnJob.frame.size.width, self.btnConfirm.frame.size.height);
        }
    }
    else
    {
        if(isType==3)
        {
            self.lblDiagnosticList.text = NSLocalizedString(@"REMOTE_TYPES", nil);
            
            [self.btnSkip setTitle:NSLocalizedString(@"GO_TO_FOLLOW_UP", nil) forState:UIControlStateNormal];
            [self.btnBack setTitle:NSLocalizedString(@"GO_TO_NORMAL", nil) forState:UIControlStateNormal];
            
            self.btnBack.hidden=NO;
            self.btnJob.hidden=YES;
            self.btnSkip.hidden=NO;
            
            self.btnBack.frame = frameForBackButton;
            
            self.btnConfirm.frame = CGRectMake(self.btnBack.frame.size.width+self.btnBack.frame.origin.x+1, frameForBackButton.origin.y, self.view.frame.size.width/3-1, self.btnConfirm.frame.size.height);
            
            self.btnSkip.frame = CGRectMake(self.btnConfirm.frame.size.width+self.btnConfirm.frame.origin.x+1, self.btnSkip.frame.origin.y, self.view.frame.size.width/3, self.btnSkip.frame.size.height);
            
            isType=2;
            
            if(arrForRemoteTypes.count!=0)
            {
                self.tableForDaignosticList.hidden=NO;
                self.imgNoItems.hidden=YES;
                [self.tableForDaignosticList reloadData];
            }
            else
            {
                self.tableForDaignosticList.hidden=YES;
                self.imgNoItems.hidden=NO;
            }
        }
        else if (isType==2)
        {
            self.lblDiagnosticList.text = NSLocalizedString(@"NORMAL_TYPES", nil);
            [self.btnSkip setTitle:NSLocalizedString(@"GO_TO_REMOTE", nil) forState:UIControlStateNormal];
            
            isType=1;
            
            self.btnBack.hidden=NO;
            self.btnBack.frame = frameForBackButton;
            
            self.btnConfirm.frame = CGRectMake(self.btnBack.frame.size.width+self.btnBack.frame.origin.x+1, frameForBackButton.origin.y, self.view.frame.size.width/3-1, self.btnConfirm.frame.size.height);
            
            self.btnSkip.frame = CGRectMake(self.btnConfirm.frame.size.width+self.btnConfirm.frame.origin.x+1, self.btnSkip.frame.origin.y, self.view.frame.size.width/3, self.btnSkip.frame.size.height);
            
            [self.btnBack setTitle:NSLocalizedString(@"GOT_TO_DIAGNOSTIC", nil) forState:UIControlStateNormal];
            
            if(arrForNormalTypes.count>0)
            {
                [self.tableForDaignosticList reloadData];
                self.tableForDaignosticList.hidden=NO;
                self.imgNoItems.hidden=YES;
            }
            else
            {
                self.tableForDaignosticList.hidden=YES;
                self.imgNoItems.hidden=NO;
            }
            
        }
        else if (isType==1)
        {
            isDiagnostic = YES;
            self.imgNoItems.hidden=YES;
            [self.tableForDaignosticList reloadData];
            self.tableForDaignosticList.frame = CGRectMake(self.tableForDaignosticList.frame.origin.x, self.tableForDaignosticList.frame.origin.y, self.tableForDaignosticList.frame.size.width, self.btnDiagnosticList.frame.origin.y-self.btnDiagnosticList.frame.size.height-15);
            self.btnDiagnosticList.hidden=NO;
            self.btnSkip.hidden=YES;
            self.btnBack.hidden=YES;
            self.btnConfirm.hidden=YES;
            self.tableForDaignosticList.hidden=NO;
            self.viewForPatientInfo.hidden=YES;
            self.viewForDiagnosticList.hidden=NO;
            self.lblDiagnosticList.text = NSLocalizedString(@"DIAGNOSTIC_LIST", nil);
            
            [self.tableForDaignosticList reloadData];
        }
    }
    
    [arrForSelected removeAllObjects];
    [arrTypeID removeAllObjects];
    
    [APPDELEGATE hideLoadingView];
}

-(void)jobDone
{
    strUserId=[PREF objectForKey:PREF_USER_ID];
    strUserToken=[PREF objectForKey:PREF_USER_TOKEN];
    strRequsetId=[PREF objectForKey:PREF_REQUEST_ID];
    
    if (strRequsetId!=nil)
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_WALK_COMPLETED withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     dictBillInfo=[response valueForKey:@"bill"];
                     
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"JOB_DONE", nil)];
                     if ([[dictBillInfo valueForKey:@"payment_type"] integerValue]==1)
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString(@"Please collect cash from client for your trip", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:Nil, nil];
                         alert.tag=111;
                         [alert show];
                     }
                     else
                     {
                         [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
                         
                         [self  performSegueWithIdentifier:@"jobToFeedback" sender:self];
                     }
                 }
                 else
                 {
                     if ([[response valueForKey:@"error_code"] integerValue]==406)
                     {
                         [self performSegueWithIdentifier:@"logout" sender:self];
                     }
                     else
                     {
                         UIAlertView *alert=[[UIAlertView alloc]initWithTitle:nil message:NSLocalizedString([[response valueForKey:@"error_messages"]objectAtIndex:0], nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:Nil, nil];
                         [alert show];
                     }
                 }
             }
         }];
    }
}

#pragma mark -
#pragma mark - Action mehtods

- (IBAction)onClickJobDone:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"JOB_DONE_ALERT", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"No", nil) otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
     alert.tag=333;
     [alert show];
}

- (IBAction)onClickSelectPhoto:(id)sender
{
    self.viewForPicker.hidden=YES;
    [self.view endEditing:YES];
    
    UIActionSheet *action=[[UIActionSheet alloc]initWithTitle:Nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", nil) ,NSLocalizedString(@"Select Image", nil) , nil];
    [action showInView:self.view];
}

- (IBAction)onClickCancelPicker:(id)sender
{
    self.viewForPicker.hidden=YES;
}

- (IBAction)onClickDonePicker:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    NSString *strForTime = [dateFormatter stringFromDate:self.datePicker.date];
    
    self.txtPetDob.text = strForTime;
    self.viewForPicker.hidden=YES;
    
    strBirthDateOfPet = [NSString stringWithFormat:@"%@",[[UtilityClass sharedObject] DateToString:self.datePicker.date]];
}

- (IBAction)onClickDoneDetails:(id)sender
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        if([self.txtPetBreed.text isEqualToString:NSLocalizedString(@"ENTER_PET_BREED", nil)])
            [dict setValue:@"" forKey:@"breed"];
        else
            [dict setValue:self.txtPetBreed.text forKey:@"breed"];
        
        [dict setValue:self.txtPetName.text forKey:@"name"];
        [dict setValue:self.txtPetWeight.text forKey:@"weight"];
        [dict setValue:self.txtFormerVet.text forKey:@"former_vet"];
        [dict setValue:strRequsetId forKey:PARAM_REQUEST_ID];
        [dict setValue:strUserId forKey:PARAM_ID];
        [dict setValue:strUserToken forKey:PARAM_TOKEN];
        [dict setValue:strPetDetailsId forKey:@"pet_detail_id"];
        
        NSString *strForTime = [self DateConverterReverse:self.txtPetDob.text];
        [dict setValue:strForTime forKey:@"birth_date"];
        
        NSLog(@"Dict data = %@",dict);
        
        AFNHelper *afn = [[AFNHelper alloc] initWithRequestMethod:POST_METHOD];
        
        UIImage *imgUpload = [[UtilityClass sharedObject]scaleAndRotateImage:self.imgForPet.image];
        
        [afn getDataFromPath:FILE_PET_INFO withParamDataImage:dict andImage:imgUpload withBlock:^(id response, NSError *error)
         {
            if(response)
            {
                NSLog(@"Pet Deails = %@",response);
                
                if([[response valueForKey:@"success"] integerValue]==1)
                {
                    [self.view endEditing:YES];
                    strPetDetailsId = [[response valueForKey:@"pet_details"] valueForKey:@"pet_detail_id"];
                    self.viewForPatientInfo.hidden=YES;
                    self.viewForDiagnosticList.hidden=NO;
                    self.tableForDaignosticList.frame = CGRectMake(self.tableForDaignosticList.frame.origin.x, self.tableForDaignosticList.frame.origin.y, self.tableForDaignosticList.frame.size.width, self.btnDiagnosticList.frame.origin.y-self.btnDiagnosticList.frame.size.height-15);
                    self.btnBack.hidden=YES;
                    self.btnConfirm.frame = CGRectMake(self.btnBack.frame.size.width+self.btnBack.frame.origin.x+1, self.btnConfirm.frame.origin.y, self.view.frame.size.width/3-1, self.btnConfirm.frame.size.height);
                    self.btnSkip.frame = CGRectMake(self.btnConfirm.frame.size.width+self.btnConfirm.frame.origin.x+1, self.btnSkip.frame.origin.y, self.view.frame.size.width/3, self.btnSkip.frame.size.height);
                }
                else
                {
                    if ([[response valueForKey:@"error_code"] integerValue]==406)
                    {
                        [self performSegueWithIdentifier:@"logout" sender:self];
                    }
                }
            }
             [APPDELEGATE hideLoadingView];
        }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)onClickSelectDiagnosticList:(id)sender
{
    if(arrForSelected.count!=0)
    {
        NSString *strCode = [arrForSelected componentsJoinedByString:@","];
        
        NSLog(@"strCode = %@",strCode);
        
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        [PREF setValue:strCode forKey:PREF_DIAGNOSTIC_CODE];
        [PREF synchronize];
        
        [self getTypes:strCode];
    }
    else
        [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_SELECT_DIAGNOSTIC", nil)];
}

- (IBAction)onClickConfirm:(id)sender
{
    if(arrTypeID.count!=0)
    {
        if([APPDELEGATE connected])
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
            
            NSString *strType = [arrTypeID componentsJoinedByString:@","];
            
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc] init];
            [dictParam setObject:strUserId forKey:PARAM_ID];
            [dictParam setObject:strUserToken forKey:PARAM_TOKEN];
            [dictParam setObject:strRequsetId forKey:PARAM_REQUEST_ID];
            [dictParam setObject:strType forKey:PARAM_TYPES];
            
            if(isType==1)
                [dictParam setObject:@"0" forKey:PARAM_OFFER_TYPE];
            
            else if(isType==2)
                [dictParam setObject:@"1" forKey:PARAM_OFFER_TYPE];
            
            else
                [dictParam setObject:@"2" forKey:PARAM_OFFER_TYPE];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CONFIRM_TYPES withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         NSLog(@"Confrim Types = %@",response);
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"WAIT_FOR_CONFIRMATION", nil)];
                         
                         NSRunLoop *runloop = [NSRunLoop currentRunLoop];
                         self.timerForGetRequest = [NSTimer scheduledTimerWithTimeInterval:7.0 target:self selector:@selector(getRequest) userInfo:nil repeats:YES];
                         [runloop addTimer:self.timerForGetRequest forMode:NSRunLoopCommonModes];
                         [runloop addTimer:self.timerForGetRequest forMode:UITrackingRunLoopMode];
                     }
                     else
                     {
                         if ([[response valueForKey:@"error_code"] integerValue]==406)
                         {
                             [self.timerForGetRequest invalidate];
                            self.timerForGetRequest = nil;
                             [self performSegueWithIdentifier:@"logout" sender:self];
                         }
                         else
                         {
                             NSString *str=[NSString stringWithFormat:@"%@",[response valueForKey:@"error"]];
                             UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[afn getErrorMessage:str] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                             [alert show];
                         }
                     }
                 }
                 NSLog(@"RESPONSE --> %@",response);
             }];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        if(isType==3)
            [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_SELECT_FOLLOW_UP_PROVIDER", nil)];
        else
        [APPDELEGATE showToastMessage:NSLocalizedString(@"PLEASE_SELECT_TYPE", nil)];
    }
}

- (IBAction)onClickSkip:(id)sender
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        [self setTableData:0];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)onClickBack:(id)sender
{
    if([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        [self setTableData:1];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)onClickBreed:(id)sender
{
    [self.view endEditing:YES];
    [self.tableForBreeds reloadData];
    self.viewForBreeds.hidden=NO;
}

#pragma mark -
#pragma mark tableview

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.tableForBreeds)
    {
        if(isCat)
            return arrForDog.count;
        else
            return arrForCat.count;
    }
    else
    {
        if(isDiagnostic==YES)
            return arrForDiagnositcLists.count;
        else
        {
            if(isType==1)
            {
                self.tableForDaignosticList.allowsMultipleSelection=YES;
                return arrForNormalTypes.count;
            }
            
            else if(isType==2)
            {
                self.tableForDaignosticList.allowsMultipleSelection=YES;
                return arrForRemoteTypes.count;
            }
            
            else
            {
                self.tableForDaignosticList.allowsMultipleSelection=NO;
                return arrForFollowUpProvider.count;
            }
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableForBreeds)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if(cell == nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        }
        
        if(isCat)
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[arrForDog objectAtIndex:indexPath.row]];
        else
            cell.textLabel.text = [NSString stringWithFormat:@"%@",[arrForCat objectAtIndex:indexPath.row]];
        
        cell.textLabel.textColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor lightGrayColor];
        
        return cell;
    }
    else
    {
        if(isDiagnostic==YES)
        {
            TypesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"types" forIndexPath:indexPath];
            
            if (cell==nil)
            {
                cell=[[TypesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"types"];
            }
            
            NSMutableDictionary *dictForType = [[NSMutableDictionary alloc] init];
            dictForType = [arrForDiagnositcLists objectAtIndex:indexPath.row];
            
            cell.lblName.text = [dictForType valueForKey:@"name"];
            cell.lblPrice.hidden=YES;
            cell.lblTypeName.hidden=YES;
            cell.lblName.hidden=NO;
            
            // for back to the list selected cell will be dispalyed
            
            NSString *strSelectedArray = [PREF objectForKey:PREF_DIAGNOSTIC_CODE];
            
            if(strSelectedArray.length>0)
            {
                NSArray *arrSelectedId = [strSelectedArray componentsSeparatedByString:@","];
            
                if([arrSelectedId containsObject:[[arrForDiagnositcLists objectAtIndex:indexPath.row] valueForKey:@"code"]])
                {
                    cell.lblBgLine.backgroundColor = [UIColor blackColor];
                    cell.backgroundColor = [UIColor clearColor];
                    cell.backgroundColor = [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0];
                    cell.lblBgLine.backgroundColor = [UIColor blackColor];
                    
                    //when we scroll table view at thta time not to add object which already  in that array
                    
                    if(![arrForSelected containsObject:[[arrForDiagnositcLists objectAtIndex:indexPath.row] valueForKey:@"code"]])
                        [arrForSelected addObject:[[arrForDiagnositcLists objectAtIndex:indexPath.row] valueForKey:@"code"]];
                    
                    [self.tableForDaignosticList selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
            NSArray *selectedCells = [self.tableForDaignosticList indexPathsForSelectedRows];
            for (NSIndexPath *index in selectedCells)
            {
                if (indexPath==index)
                {
                    cell.backgroundColor = [UIColor colorWithRed:210.0f/255.0f green:210.0f/255.0f blue:210.0f/255.0f alpha:1.0];
                    cell.lblBgLine.backgroundColor = [UIColor blackColor];
                }
            }
            
            return cell;
        }
        else
        {
            TypesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"types" forIndexPath:indexPath];
            
            if (cell==nil)
            {
                cell=[[TypesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"types"];
            }
            
            NSMutableDictionary *dictForType = [[NSMutableDictionary alloc] init];
            
            if(isType==1)
                dictForType = [arrForNormalTypes objectAtIndex:indexPath.row];
            
            else if(isType==2)
                dictForType = [arrForRemoteTypes objectAtIndex:indexPath.row];
            
            else
                dictForType = [arrForFollowUpProvider objectAtIndex:indexPath.row];
            
            NSString *strCurrency = [dictForType valueForKey:@"currency"];
            
            cell.lblName.hidden=YES;
            cell.lblTypeName.hidden=NO;
            cell.lblPrice.hidden=NO;
            cell.lblTypeName.text = [dictForType valueForKey:@"name"];
            cell.lblPrice.text = [NSString stringWithFormat:@"%@ %@",strCurrency,[dictForType valueForKey:@"base_price"]];
            cell.lblBgLine.backgroundColor = [UIColor blackColor];
            cell.backgroundColor = [UIColor clearColor];
            
            return cell;
        }
    }
    
    [self.tableForDaignosticList deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == self.tableForBreeds)
    {
        NSString *str;
        
        if(isCat)
            str = [NSString stringWithFormat:@"%@",[arrForDog objectAtIndex:indexPath.row]];
        else
            str = [NSString stringWithFormat:@"%@",[arrForCat objectAtIndex:indexPath.row]];
        
        self.txtPetBreed.text = str;
        self.viewForBreeds.hidden=YES;
    }
    else
    {
        TypesCell *Cell=[self.tableForDaignosticList cellForRowAtIndexPath:indexPath];
        
        if(isDiagnostic==YES)
        {
            
            Cell.backgroundColor = [UIColor darkGrayColor];
            Cell.lblBgLine.backgroundColor = [UIColor blackColor];
            
            NSString *strCode = [NSString stringWithFormat:@"%@",[[arrForDiagnositcLists objectAtIndex:indexPath.row] valueForKey:@"code"]];
            
            [arrForSelected addObject:strCode];
            
            NSLog(@"selected code = %@",arrForSelected);
        }
        else
        {
            Cell.backgroundColor = [UIColor darkGrayColor];
            Cell.lblBgLine.backgroundColor = [UIColor blackColor];
            
            NSString *strTypeid;
            
            if(isType==1)
            {
                strTypeid = [NSString stringWithFormat:@"%@",[[arrForNormalTypes objectAtIndex:indexPath.row] valueForKey:@"id"]];
                Cell.backgroundColor = [UIColor darkGrayColor];
                Cell.lblBgLine.backgroundColor = [UIColor blackColor];
                [arrTypeID addObject:strTypeid];
            }
            
            else if(isType==2)
            {
                strTypeid = [NSString stringWithFormat:@"%@",[[arrForRemoteTypes objectAtIndex:indexPath.row] valueForKey:@"id"]];
                Cell.backgroundColor = [UIColor darkGrayColor];
                Cell.lblBgLine.backgroundColor = [UIColor blackColor];
                [arrTypeID addObject:strTypeid];
            }
            else
            {
                strTypeid = [NSString stringWithFormat:@"%@",[[arrForFollowUpProvider objectAtIndex:indexPath.row] valueForKey:@"id"]];
                
                Cell.lblBgLine.backgroundColor = [UIColor blackColor];
                
                NSArray *selectedCells = [self.tableForDaignosticList indexPathsForSelectedRows];
                for (NSIndexPath *index in selectedCells)
                {
                    if (indexPath==index)
                    {
                        Cell.backgroundColor = [UIColor darkGrayColor];
                        [arrTypeID removeAllObjects];
                        [arrTypeID addObject:strTypeid];
                    }
                }
                
                NSLog(@"selected id = %@",arrTypeID);
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TypesCell *Cell=[self.tableForDaignosticList cellForRowAtIndexPath:indexPath];
    
    if(isDiagnostic==YES)
    {
        Cell.backgroundColor = [UIColor whiteColor];
        Cell.lblBgLine.backgroundColor = [UIColor blackColor];
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[[arrForDiagnositcLists objectAtIndex:indexPath.row] valueForKey:@"code"]];
        [arrForSelected removeObject:strCode];
        
        NSLog(@"selected code = %@",arrForSelected);
    }
    else
    {
        Cell.backgroundColor = [UIColor whiteColor];
        Cell.lblBgLine.backgroundColor = [UIColor blackColor];

        NSString *strTypeid;
        
        if(isType==1)
            strTypeid = [NSString stringWithFormat:@"%@",[[arrForNormalTypes objectAtIndex:indexPath.row] valueForKey:@"id"]];
        
        else if(isType==2)
            strTypeid = [NSString stringWithFormat:@"%@",[[arrForRemoteTypes objectAtIndex:indexPath.row] valueForKey:@"id"]];
        
        else
            strTypeid = [NSString stringWithFormat:@"%@",[[arrForFollowUpProvider objectAtIndex:indexPath.row] valueForKey:@"id"]];
        
        [arrTypeID removeObject:strTypeid];
    }
}

#pragma mark -
#pragma mark - Date Converter

-(NSString *)DateConverter:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"dd/MM/yyyy"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

-(NSString *)DateConverterReverse:(NSString *)dateStr
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
    dateStr = [dateFormatters stringFromDate: date];
    
    return dateStr;
}

#pragma mark -
#pragma mark - UIActionSheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:[self TakePhoto];
            break;
        case 1:[self chooseFromLibaray];
            break;
        case 2:
            break;
    }
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==111)
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        
        [self performSegueWithIdentifier:@"jobToFeedback" sender:self];
    }
    if(alertView.tag==222)
    {
        [self ConfirmUserResponse];
    }
    if(alertView.tag==333)
    {
        if(buttonIndex == 1)
        {
            [self jobDone];
        }
    }
}

#pragma mark -
#pragma mark - Take Photo/Take From Library

-(void)chooseFromLibaray
{
    // Set up the image picker controller and add it to the view
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    imagePickerController.allowsEditing=YES;
    imagePickerController.view.tag = 102;
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:^{
    }];
}

-(void)TakePhoto
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate =self;
        imagePickerController.allowsEditing=YES;
        imagePickerController.view.tag = 102;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{
            
        }];
    }
    else
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
    }
}

#pragma mark -
#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.imgForPet applyRoundedCornersFull];
    self.imgForPet.contentMode = UIViewContentModeScaleAspectFill;
    self.imgForPet.clipsToBounds = YES;
    self.imgForPet.image=[info objectForKey:UIImagePickerControllerEditedImage];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark-
#pragma mark- Text Field Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return  YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == self.txtPetDob)
    {
        [self.view endEditing:YES];
        self.viewForBreeds.hidden=YES;
        self.viewForPicker.hidden=NO;
        self.datePicker.datePickerMode = UIDatePickerModeDate;
        self.datePicker.maximumDate=[NSDate date];
        [self.datePicker setDate:[NSDate date]];
        return NO;
    }
    if(textField == self.txtPetBreed)
    {
        [self.tableForBreeds reloadData];
        [self.view endEditing:YES];
        self.viewForPicker.hidden=YES;
        self.viewForBreeds.hidden=NO;
        return NO;
    }
    else
    {
        self.viewForPicker.hidden=YES;
        return YES;
    }
}

@end
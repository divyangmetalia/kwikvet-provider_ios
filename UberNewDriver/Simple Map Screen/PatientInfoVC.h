//
//  PatientInfoVC.h
//  KwikVet for VETS
//
//  Created by Elluminati Macbook Pro 2 on 9/30/16.
//  Copyright © 2016 Deep Gami. All rights reserved.
//

#import "BaseVC.h"

@interface PatientInfoVC : BaseVC<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnJob;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelPicker;
@property (weak, nonatomic) IBOutlet UIButton *btnDonePicker;
@property (weak, nonatomic) IBOutlet UIButton *btnDoneDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnDiagnosticList;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property(nonatomic, strong) NSTimer *timerForGetRequest;
@property (weak, nonatomic) IBOutlet UITableView *tableForBreeds;

@property (weak, nonatomic) IBOutlet UITableView *tableForDaignosticList;

@property (weak, nonatomic) IBOutlet UIImageView *imgForPet;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoItems;


@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UILabel *lblPatientInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblPetName;
@property (weak, nonatomic) IBOutlet UILabel *lblPetBreed;
@property (weak, nonatomic) IBOutlet UILabel *lblPetDob;
@property (weak, nonatomic) IBOutlet UILabel *lblPetWeight;
@property (weak, nonatomic) IBOutlet UILabel *lblPetPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblPetFormerVet;
@property (weak, nonatomic) IBOutlet UILabel *lblDiagnosticList;
@property (weak, nonatomic) IBOutlet UILabel *lblUnderLine;

@property (weak, nonatomic) IBOutlet UITextField *txtPetName;
@property (weak, nonatomic) IBOutlet UITextField *txtPetDob;
@property (weak, nonatomic) IBOutlet UITextField *txtPetWeight;
@property (weak, nonatomic) IBOutlet UITextField *txtFormerVet;
@property (weak, nonatomic) IBOutlet UITextField *txtPetBreed;

@property (weak, nonatomic) IBOutlet UIView *viewForPatientInfo;
@property (weak, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UIView *viewForDiagnosticList;
@property (weak, nonatomic) IBOutlet UIView *viewForBreeds;


- (IBAction)onClickJobDone:(id)sender;
- (IBAction)onClickSelectPhoto:(id)sender;
- (IBAction)onClickCancelPicker:(id)sender;
- (IBAction)onClickDonePicker:(id)sender;
- (IBAction)onClickDoneDetails:(id)sender;
- (IBAction)onClickSelectDiagnosticList:(id)sender;
- (IBAction)onClickConfirm:(id)sender;
- (IBAction)onClickSkip:(id)sender;
- (IBAction)onClickBack:(id)sender;

@end
